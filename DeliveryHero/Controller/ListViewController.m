//
//  ListViewController.m
//  DeliveryHero
//
//  Created by Johnykutty Mathew on 14/10/15.
//  Copyright (c) 2015 Johnykutty Mathew. All rights reserved.
//

#import "ListViewController.h"
#import "WomenClothingService.h"
#import "ListServiceInput.h"
#import "CoreDatahandler.h"
#import "Product.h"
#import "ProductCell.h"

#import <DZNEmptyDataSet/UIScrollView+EmptyDataSet.h>

@interface ListViewController () <NSFetchedResultsControllerDelegate, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate> {
    BOOL isFullLoaded;
    BOOL isLoading;
}
@property (strong, nonatomic) IBOutlet UIView *headerView;
@property (nonatomic, strong) NSString *emptyMessage;
@property (weak, nonatomic) IBOutlet UISegmentedControl *sortKeySelector;
@property (weak, nonatomic) IBOutlet UIButton *sortOrderSelector;
@property (nonatomic, strong) ListServiceInput *serviceInput;
@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;

@end

@implementation ListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.emptyMessage = @"Loading...";

    self.tableView.tableHeaderView = nil;
    
    self.tableView.emptyDataSetSource = self;
    self.tableView.emptyDataSetDelegate = self;
    
    self.tableView.tableFooterView = [UIView new];
    
    [self loadData];
}

- (NSFetchedResultsController *)fetchedResultsController {
    if (_fetchedResultsController) {
        return _fetchedResultsController;
    }
    
    NSManagedObjectContext *context = [[CoreDatahandler sharedInstance] managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Product"];

    NSSortDescriptor *sortDescriptor = [self.serviceInput sortDescriptor];
    [fetchRequest setSortDescriptors:@[sortDescriptor]];
    
    NSFetchedResultsController *controller = [[NSFetchedResultsController alloc]
                                              initWithFetchRequest:fetchRequest
                                              managedObjectContext:context
                                              sectionNameKeyPath:nil
                                              cacheName:nil];
    controller.delegate = self;
    _fetchedResultsController = controller;
    return _fetchedResultsController;
}

- (void)loadData {
    self.emptyMessage = @"Loading...";
    isLoading = true;

    NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error]) {
        NSLog(@"Failed to fetch %@",error);
    }
    
    WomenClothingService *service = [WomenClothingService new];
    
    [service loadWithInput:self.serviceInput success:^(NSArray *products) {
        //Since we are using FRC do nothing wit array
        [self.tableView flashScrollIndicators];
        isLoading = false;
        isFullLoaded = !products.count;
    } failure:^(NSError *error) {
        isLoading = false;

        self.emptyMessage = error ? error.localizedDescription : @"";
        if ([self.tableView numberOfRowsInSection:0] == 0) {
            [self.tableView reloadData];
        }
    }];
}

- (ListServiceInput *)serviceInput {
    if (_serviceInput) {
        return _serviceInput;
    }
    _serviceInput = [ListServiceInput new];
    _serviceInput.sortIsAscending = self.sortOrderSelector.isSelected;
    _serviceInput.sortBy = self.sortOrderSelector.isSelected;
    return _serviceInput;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)resetPage {
    self.serviceInput.page = 1;
    self.fetchedResultsController = nil;
    isFullLoaded = NO;
    //using -55 since table view header is there
    [self.tableView setContentOffset:CGPointMake(0.f, -55.f) animated:YES];
}

- (IBAction)changeSortKey:(UISegmentedControl *)sender {
    self.serviceInput.sortBy = sender.selectedSegmentIndex;
    [self resetPage];

    [self loadData];
}

- (IBAction)toggleSortOrder:(UIButton *)sender {
    sender.selected = !sender.selected;
    self.serviceInput.sortIsAscending = sender.selected;
    [self resetPage];

    [self loadData];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    [self checkAndReload:scrollView];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    [self checkAndReload:scrollView];
}

- (void)checkAndReload:(UIScrollView *)scrollView {
    CGFloat currentOffset = scrollView.contentOffset.y;
    CGFloat maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height;
    if ((!isFullLoaded) && (!isLoading) && (maximumOffset - currentOffset <= 30.0)) {
        NSLog(@"reload");
        self.serviceInput.page = self.serviceInput.page + 1;
        [self loadData];
    }
}


- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView {
    return [[NSAttributedString alloc] initWithString:self.emptyMessage];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section {
    if ([[self.fetchedResultsController sections] count] > 0) {
        id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
        return [sectionInfo numberOfObjects];
    } else
        return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ProductCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ProductCell" forIndexPath:indexPath];
    Product *product = [self.fetchedResultsController objectAtIndexPath:indexPath];
    [cell setProduct:product];
    return cell;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return self.headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 55.f;
}

#pragma mark - NSFetchedResultsController delegate methods
- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath {
    
    UITableView *tableView = self.tableView;
    
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id )sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
    
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        default:
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView endUpdates];
}
@end
