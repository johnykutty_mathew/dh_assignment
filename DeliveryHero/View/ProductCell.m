//
//  ProductCell.m
//  DeliveryHero
//
//  Created by Johnykutty Mathew on 15/10/15.
//  Copyright (c) 2015 Johnykutty Mathew. All rights reserved.
//

#import "ProductCell.h"
#import "Product+Extra.h"
#import "Image.h"

#import <SDWebImage/UIImageView+WebCache.h>

@interface ProductCell ()
@property (weak, nonatomic) IBOutlet UIImageView *thumbnailView;
@property (weak, nonatomic) IBOutlet UILabel *productNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *brandNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;

@end

@implementation ProductCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setProduct:(Product *)product {
    _product = product;
    NSString *imagePath = [[product defaultImage] path];
    [self.thumbnailView sd_setImageWithURL:[NSURL URLWithString:imagePath] placeholderImage:[UIImage imageNamed:@"Placeholder"]];
    self.productNameLabel.text = product.productName;
    self.brandNameLabel.text = [@"by " stringByAppendingFormat:@"%@",product.brand];
    self.priceLabel.text = [product.price description];
    
}
@end
