//
//  ProductCell.h
//  DeliveryHero
//
//  Created by Johnykutty Mathew on 15/10/15.
//  Copyright (c) 2015 Johnykutty Mathew. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Product;

@interface ProductCell : UITableViewCell
@property (nonatomic, strong) Product *product;
@end
