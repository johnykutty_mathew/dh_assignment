//
//  ListServiceInput.h
//  DeliveryHero
//
//  Created by Johnykutty Mathew on 14/10/15.
//  Copyright (c) 2015 Johnykutty Mathew. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, DeliveryHeroSortBy) {
    DeliveryHeroSortByPopularity,
    DeliveryHeroSortByName,
    DeliveryHeroSortByPrice,
    DeliveryHeroSortByBrand
};

@interface ListServiceInput : NSObject
@property (nonatomic, strong) NSString *path;
@property (nonatomic, assign) NSInteger maxItems;
@property (nonatomic, assign) NSInteger page;
@property (nonatomic, assign) DeliveryHeroSortBy sortBy;
@property (nonatomic, assign) BOOL sortIsAscending;

- (NSDictionary *)requestDictionary;

- (NSSortDescriptor *)sortDescriptor;
- (NSPredicate *)predicate;
@end
