//
//  ListServiceInput.m
//  DeliveryHero
//
//  Created by Johnykutty Mathew on 14/10/15.
//  Copyright (c) 2015 Johnykutty Mathew. All rights reserved.
//

#import "ListServiceInput.h"

@implementation ListServiceInput

- (NSDictionary *)requestDictionary {
    NSMutableDictionary *params = [NSMutableDictionary new];
    
    [params setValue:@(self.maxItems) forKey:@"maxItems"];
    [params setValue:@(self.page) forKey:@"page"];
    [params setValue:[self sort] forKey:@"sort"];
    [params setValue:[self sortOrder] forKey:@"dir"];
    
    return params;
}

- (NSInteger)maxItems {
    return (_maxItems > 0) ? _maxItems : 5;
}

- (NSInteger)page {
    return (_page > 0) ? _page : 1;
}

- (NSString *)sort {
    NSString *sortKey = @"popularity";
    switch (_sortBy) {
        case DeliveryHeroSortByPopularity:
            sortKey = @"popularity";
            break;
        case DeliveryHeroSortByName:
            sortKey = @"name";
            break;
        case DeliveryHeroSortByPrice:
            sortKey = @"price";
            break;
        case DeliveryHeroSortByBrand:
            sortKey = @"brand";
            break;
        default:
            break;
    }
    return sortKey;
}


- (NSSortDescriptor *)sortDescriptor {
    NSString *sortKey = [self sort];
    sortKey = [sortKey isEqualToString:@"name"] ? @"productName" : sortKey;
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:sortKey ascending:self.sortIsAscending];
    return sortDescriptor;
}

- (NSString *)sortOrder {
    return _sortIsAscending ? @"asc" : @"desc";
}
@end
