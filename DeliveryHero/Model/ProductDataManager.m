//
//  ProductDataManager.m
//  DeliveryHero
//
//  Created by Johnykutty Mathew on 15/10/15.
//  Copyright (c) 2015 Johnykutty Mathew. All rights reserved.
//

#import "ProductDataManager.h"
#import "Product.h"
#import "CoreDatahandler.h"
#import "Image.h"

@implementation ProductDataManager
- (Product *)productWithID:(NSNumber *)productID {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"productID == %@",productID];
    
    NSManagedObjectContext *managedObjectContext = [[CoreDatahandler sharedInstance] managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"Product" inManagedObjectContext:managedObjectContext];
    [fetchRequest setEntity:entityDescription];
    [fetchRequest setPredicate:predicate];
    
    NSError * error = nil;
    NSArray *fetchedObjects = [managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    if([fetchedObjects count]) {
        return fetchedObjects.firstObject;
    }

    Product *product = (Product *)[[CoreDatahandler sharedInstance] insertNewObjectForEntityForName:@"Product"];
    product.productID = productID;
    return product;
}

- (Product *)productWithDict:(NSDictionary *)dict {
    NSNumber *pID = [self numberFromObject:dict[@"id"]];
    Product *product = [self productWithID:pID];
    [self mapDict:dict toProduct:product];
    return product;
}

- (void)mapDict:(NSDictionary *)dict toProduct:(Product *)product {
    product.productName = [dict valueForKeyPath:@"data.name"];
    product.brand = [dict valueForKeyPath:@"data.brand"];
    product.price = [self numberFromObject:[dict valueForKeyPath:@"data.price"]];
    
    NSArray *images = dict[@"images"];
    for (NSDictionary *imageDict in images) {
        Image *image = (Image *)[[CoreDatahandler sharedInstance] insertNewObjectForEntityForName:@"Image"];
        [self mapDict:imageDict toImage:image];
        [product addImagesObject:image];
    }
}
- (void)mapDict:(NSDictionary *)dict toImage:(Image *)image {
    image.path = dict[@"path"];
    image.format = dict[@"format"];
    image.width = [self numberFromObject:dict[@"width"]];
    image.height = [self numberFromObject:dict[@"height"]];
    image.isDefault = @([[dict[@"default"] description] boolValue]);

}

- (NSNumber *)numberFromObject:(id)object {
    return object ? @([[object description] doubleValue]) : nil;
}

@end
