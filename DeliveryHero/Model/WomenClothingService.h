//
//  WomenClothingService.h
//  DeliveryHero
//
//  Created by Johnykutty Mathew on 14/10/15.
//  Copyright (c) 2015 Johnykutty Mathew. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "WebserviceClient.h"

@class ListServiceInput;

@interface WomenClothingService : NSObject

- (void)loadWithInput:(ListServiceInput *)input success:(ListOperationSuccessBlock)successHandler failure:(OperationFailureBlock)failure;
@end
