//
//  WebserviceClient.m
//  DeliveryHero
//
//  Created by Johnykutty Mathew on 14/10/15.
//  Copyright (c) 2015 Johnykutty Mathew. All rights reserved.
//

#import "WebserviceClient.h"
#import "ListServiceInput.h"

#import <AFNetworking/AFNetworking.h>
#import <AFNetworking/AFNetworkActivityIndicatorManager.h>

NSString *const kBaseURL = @"https://www.zalora.com.my/mobile-api/";

@interface WebserviceClient ()
@end

@implementation WebserviceClient

+ (WebserviceClient *)sharedInstance {
    static WebserviceClient *sharedClient;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        [[AFNetworkActivityIndicatorManager sharedManager] setEnabled:YES];
        sharedClient = [[self alloc] initWithBaseURL:[NSURL URLWithString:kBaseURL]];
        
    });
    return sharedClient;
}

- (void)handleResponse:(id)response success:(ListOperationSuccessBlock)successHandler failure:(OperationFailureBlock)failureHandler {
    
    NSDictionary *responseDict = [response isKindOfClass:[NSDictionary class]] ? response : nil;
    if ([responseDict[@"success"] boolValue]) {
        successHandler([responseDict valueForKeyPath:@"metadata.results"]);
    }
    else {
        NSError *error = [NSError errorWithDomain:@"DHErrorDomain" code:-100 userInfo:@{NSLocalizedDescriptionKey : @"Loading failed"}];
        failureHandler(error);
    }
}



- (void)loadListWithInput:(ListServiceInput *)input success:(ListOperationSuccessBlock)successHandler failure:(OperationFailureBlock)failureHandler {
    
    [self GET:input.path parameters:[input requestDictionary] success:^(NSURLSessionDataTask *task, id result) {
        [self handleResponse:result success:successHandler failure:failureHandler];
    } failure:^(NSURLSessionDataTask * task, NSError * error) {
        failureHandler(error);
    }];
}
@end
