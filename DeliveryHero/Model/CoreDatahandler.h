//
//  CoreDatahandler.h
//  DeliveryHero
//
//  Created by Johnykutty Mathew on 14/10/15.
//  Copyright (c) 2015 Johnykutty Mathew. All rights reserved.
//

#import <Foundation/Foundation.h>

@import CoreData;

@interface CoreDatahandler : NSObject

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

+ (CoreDatahandler *)sharedInstance;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

- (NSManagedObject *)insertNewObjectForEntityForName:(NSString *)entityName;

@end
