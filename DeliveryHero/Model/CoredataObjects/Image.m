//
//  Image.m
//  DeliveryHero
//
//  Created by Johnykutty Mathew on 15/10/15.
//  Copyright (c) 2015 Johnykutty Mathew. All rights reserved.
//

#import "Image.h"
#import "Product.h"


@implementation Image

@dynamic path;
@dynamic format;
@dynamic isDefault;
@dynamic width;
@dynamic height;
@dynamic product;

@end
