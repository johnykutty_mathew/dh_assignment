//
//  Product.m
//  DeliveryHero
//
//  Created by Johnykutty Mathew on 15/10/15.
//  Copyright (c) 2015 Johnykutty Mathew. All rights reserved.
//

#import "Product.h"
#import "Image.h"


@implementation Product

@dynamic productID;
@dynamic price;
@dynamic productName;
@dynamic brand;
@dynamic popularity;
@dynamic images;

@end
