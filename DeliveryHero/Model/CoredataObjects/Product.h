//
//  Product.h
//  DeliveryHero
//
//  Created by Johnykutty Mathew on 15/10/15.
//  Copyright (c) 2015 Johnykutty Mathew. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Image;

@interface Product : NSManagedObject

@property (nonatomic, retain) NSNumber * productID;
@property (nonatomic, retain) NSNumber * price;
@property (nonatomic, retain) NSString * productName;
@property (nonatomic, retain) NSString * brand;
@property (nonatomic, retain) NSNumber * popularity;
@property (nonatomic, retain) NSSet *images;
@end

@interface Product (CoreDataGeneratedAccessors)

- (void)addImagesObject:(Image *)value;
- (void)removeImagesObject:(Image *)value;
- (void)addImages:(NSSet *)values;
- (void)removeImages:(NSSet *)values;

@end
