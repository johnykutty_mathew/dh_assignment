//
//  Product+Extra.h
//  DeliveryHero
//
//  Created by Johnykutty Mathew on 15/10/15.
//  Copyright (c) 2015 Johnykutty Mathew. All rights reserved.
//

#import "Product.h"

@interface Product (Extra)
- (Image *)defaultImage;
@end
