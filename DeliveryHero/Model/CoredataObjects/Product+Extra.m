//
//  Product+Extra.m
//  DeliveryHero
//
//  Created by Johnykutty Mathew on 15/10/15.
//  Copyright (c) 2015 Johnykutty Mathew. All rights reserved.
//

#import "Product+Extra.h"
#import "Image.h"

@implementation Product (Extra)
- (Image *)defaultImage {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isDefault == %@",@(true)];
    NSSet *filtered = [self.images filteredSetUsingPredicate:predicate];
    return filtered.count ? [filtered anyObject] : [self.images anyObject];
}
@end
