//
//  WomenClothingService.m
//  DeliveryHero
//
//  Created by Johnykutty Mathew on 14/10/15.
//  Copyright (c) 2015 Johnykutty Mathew. All rights reserved.
//

#import "WomenClothingService.h"
#import "WebserviceClient.h"
#import "ListServiceInput.h"
#import "Product.h"
#import "ProductDataManager.h"
#import "CoreDatahandler.h"

@implementation WomenClothingService


- (void)loadWithInput:(ListServiceInput *)input success:(ListOperationSuccessBlock)successHandler failure:(OperationFailureBlock)failure {
    input.path = @"women/clothing";
    [[WebserviceClient sharedInstance] loadListWithInput:input success:^(NSArray *result) {
        [self handleResponse:result success:successHandler failure:failure];
    } failure:failure];
}

- (void)handleResponse:(NSArray *)products success:(ListOperationSuccessBlock)successHandler failure:(OperationFailureBlock)failureHandler {
    ProductDataManager *manager = [ProductDataManager new];
    NSMutableArray *productObjects = [NSMutableArray new];
    for (NSDictionary *dict in products) {
        Product *product = [manager productWithDict:dict];
        [productObjects addObject:product];
    }
    [[CoreDatahandler sharedInstance] saveContext];
    successHandler(productObjects);
}
@end
