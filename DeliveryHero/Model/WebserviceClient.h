//
//  WebserviceClient.h
//  DeliveryHero
//
//  Created by Johnykutty Mathew on 14/10/15.
//  Copyright (c) 2015 Johnykutty Mathew. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <AFNetworking/AFNetworking.h>

typedef void(^OperationFailureBlock)(NSError *error);
typedef void(^ListOperationSuccessBlock)(NSArray *result);


@class ListServiceInput;

@interface WebserviceClient : AFHTTPSessionManager

+ (WebserviceClient *)sharedInstance;

- (void)loadListWithInput:(ListServiceInput *)input success:(ListOperationSuccessBlock)successHandler failure:(OperationFailureBlock)failureHandler;

@end
