//
//  ProductDataManager.h
//  DeliveryHero
//
//  Created by Johnykutty Mathew on 15/10/15.
//  Copyright (c) 2015 Johnykutty Mathew. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Product;

@interface ProductDataManager : NSObject
- (Product *)productWithID:(NSNumber *)productID;
- (Product *)productWithDict:(NSDictionary *)dict;
@end
